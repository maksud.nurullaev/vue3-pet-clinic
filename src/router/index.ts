import { createWebHistory, createRouter } from 'vue-router'

import Home from "../components/main/Body2ContentHome.vue"
import Services from "../components/main/Body2ContentServices.vue"
import Login from "../components/main/Body2ContentLogin.vue"
import About from "../components/main/Body2ContentAbout.vue"

const routes =  [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/services',
      name: 'Services',
      component: Services
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/about',
      name: 'About',
      component: About
    }
  ]

  const router = createRouter({
    history: createWebHistory(),
    routes
  })

  export default router