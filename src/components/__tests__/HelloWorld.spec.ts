import { describe, it, expect } from "vitest";

import { mount } from "@vue/test-utils";
import AboutPage from "../main/Body2ContentAbout.vue";

describe("Body2ContentAbout", () => {
  it("renders properly", () => {
    const wrapper = mount(AboutPage);
    expect(wrapper.text()).toContain("Мы посвятили этот сайт нашему Семену");
  });
});
